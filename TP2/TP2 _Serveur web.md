# TP2 : Serveur web

## I. Base de données

```bash=
#Installer le paquet Mariadb-server
[admin@db ~]$ sudo yum install mariadb-server
[sudo] password for admin:
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.ircam.fr
 * extras: centos.crazyfrogs.org
 * updates: centos.crazyfrogs.org
base                                                                                             | 3.6 kB  00:00:00
[….]
  perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7                perl-DBD-MySQL.x86_64 0:4.023-6.el7
  perl-DBI.x86_64 0:1.627-4.el7                              perl-Data-Dumper.x86_64 0:2.145-3.el7
  perl-IO-Compress.noarch 0:2.061-2.el7                      perl-Net-Daemon.noarch 0:0.48-5.el7
  perl-PlRPC.noarch 0:0.2020-14.el7
Complete!
```
```bash=
#Activer le service MariaDB
[admin@db ~]$ systemctl start mariadb.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to manage system services or units.
Authenticating as: pierre
Password:
==== AUTHENTICATION COMPLETE ===
[admin@db ~]$ sudo systemctl enable mariadb.service
[sudo] password for admin:
Created symlink from /etc/systemd/system/multi-user.target.wants/mariadb.service to /usr/lib/systemd/system/mariadb.service
```
```bash=
#On vérifie le fonctionnement du service MariaDB
[admin@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-01-06 10:41:28 CET; 3min 55s ago
 Main PID: 1496 (mysqld_safe)
   CGroup: /system.slice/mariadb.service
           ├─1496 /bin/sh /usr/bin/mysqld_safe --basedir=/usr
           └─1661 /usr/libexec/mysqld --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib64/mysql/plugin --...
```
```bash=
#se connecter à la base de données
[admin@db ~]$ sudo mysql -u root
[sudo] password for admin:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
```bash=
#Créer une base
MariaDB [(none)]> CREATE DATABASE datacesi;
Query OK, 1 row affected (0.00 sec)
```
```bash=
#Créer un user
MariaDB [(none)]> CREATE USER 'admindata'@’10.99.99.12’ IDENTIFIED BY 'admindata';
Query OK, 0 rows affected (0.00 sec)
MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| admindata |
| root      |
| root      |
|           |
| root      |
|           |
| root      |
+-----------+
7 rows in set (0.00 sec)
```
```bash=
#Donner les droits sur la database au user
MariaDB [(none)]> GRANT all privileges on datacesi.* to admindata;
Query OK, 0 rows affected (0.00 sec)
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

```
```bash=
#Ouvrir le port 3306
[admin@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for admin:
Success
[admin@web ~]$ sudo firewall-cmd --reload
[sudo] password for admin:
success
```
```bash=
#Vérification base fonctionnelle : 
MariaDB [(none)]> show databases
    -> ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| datacesi           |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)
```
```bash=
#Se connecter ensuite en admindata
[admin@db ~]$ mysql -u admindata -p -h 10.99.99.12 -D datacesi
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
```bash=
#Passer ensuite le USER admindata en lien avec le serveur web
MariaDB [(none)]> UPDATE mysql.user SET Host='10.99.99.11' WHERE Host='%' AND User='admindata';
Query OK, 0 rows affected (0.00 sec)
Rows matched: 0  Changed: 0  Warnings: 0

MariaDB [(none)]> GRANT ALL PRIVILEGES ON datacesi.* TO 'admindata'@'10.99.99.11';
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)

```

## II. Serveur WEB

```bash=
#On installe Apache
[admin@web ~]$ sudo yum install httpd -y
[sudo] password for admin:
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.crazyfrogs.org
 * extras: centos.crazyfrogs.org
 * updates: centos.crazyfrogs.org
[…]
Dependency Installed:
  apr.x86_64 0:1.4.8-7.el7            apr-util.x86_64 0:1.5.2-6.el7       httpd-tools.x86_64 0:2.4.6-97.el7.centos
  mailcap.noarch 0:2.1.41-2.el7
Complete!
```
```bash=
#On ouvre les ports pour le http et HTTPS 
[admin@web ~]$ sudo firewall-cmd --permanent --add-service=http
success
[admin@web ~]$ sudo firewall-cmd --permanent --add-service=https
Success
[admin@web ~]$ sudo fireawall-cmd --reload
Success
```
```bash=
#On active le service Apache
[admin@web ~]$ sudo systemctl start httpd
[admin@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-01-06 13:49:48 CET; 13s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 1702 (httpd)
   Status: "Total requests: 0; Current requests/sec: 0; Current traffic:   0 B/sec"
   CGroup: /system.slice/httpd.service
           ├─1702 /usr/sbin/httpd -DFOREGROUND
           ├─1703 /usr/sbin/httpd -DFOREGROUND
           ├─1704 /usr/sbin/httpd -DFOREGROUND
           ├─1705 /usr/sbin/httpd -DFOREGROUND
           ├─1706 /usr/sbin/httpd -DFOREGROUND
           └─1707 /usr/sbin/httpd -DFOREGROUND
```
```bash=
#On rend l’activation permanente même en restart
[admin@web ~]$ sudo systemctl enable httpd
[sudo] password for admin:
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.

```
On teste ensuite depuis le navigateur : http://10.99.99.11
--- Le serveur est accessible ! 

```bash=
#On installe le langage PHP
[admin@web ~]$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
Loaded plugins: fastestmirror
remi-release-7.rpm                                                                               |  23 kB  00:00:00
Examining /var/tmp/yum-root-MeD5kt/remi-release-7.rpm: remi-release-7.9-1.el7.remi.noarch
[…]
Complete!

[admin@web ~]$ sudo yum install -y yum-utils
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
epel/x86_64/metalink
[…]
Complete !
```
```bash=
#On active le dépôt
[admin@web ~]$ sudo yum-config-manager --enable remi-php56
Loaded plugins: fastestmirror
=================================================== repo: remi-php56 ===================================================
[remi-php56]
async = True
[…]
ui_repoid_vars = releasever,
   basearch
username =
```
```bash=
#On installe PHP 5.6.40 et de librairie récurrente
[admin@web ~]$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.crazyfrogs.org
[…]
Complete !
```
```bash=
#On télécharge wget
[admin@web ~]$ sudo yum install wget -y
[sudo] password for admin:
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.crazyfrogs.org
 * epel: mirror.nl.leaseweb.net
[…]
Complete ! 
```
```bash=
#On télécharge WordPress
[admin@web ~]$ wget https://wordpress.org/latest.tar.gz
--2021-01-06 14:18:12--  https://wordpress.org/latest.tar.gz
Resolving wordpress.org (wordpress.org)... 198.143.164.252
Connecting to wordpress.org (wordpress.org)|198.143.164.252|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 15422346 (15M) [application/octet-stream]
Saving to: ‘latest.tar.gz’
100%[=======================>] 15,422,346  11.4MB/s   in 1.3s
2021-01-06 14:18:13 (11.4 MB/s) - ‘latest.tar.gz’ saved [15422346/15422346]

#On unzip le fichier téléchargé
[admin@web ~]$ tar xzvf latest.tar.gz
wordpress/
wordpress/xmlrpc.php
wordpress/wp-blog-header.php
wordpress/readme.html
[…]
wordpress/wp-trackback.php
wordpress/wp-comments-post.php
```
```bash=
#On copie l’archive dans un endroit où wordpress pourra être servi par Apache
#Je télécharge d’abord rsync pour faire ma copie avec un outil adapté
[admin@web ~]$ sudo yum install rsync -y 
[…]
Complete ! 
#Je lance ensuite la copie 
[admin@web ~]$ sudo yum install rsync -y
[…]
sent 52,081,176 bytes  received 42,856 bytes  34,749,354.67 bytes/sec
total size is 51,918,380  speedup is 1.00
```
```bash=
#On ajoute un directory a wordpress pour stocker les fichier upload
[admin@web ~]$ mkdir /var/www/html/wp-content/uploads
#Configurer Apache pour qu’il serve le dossier ou se trouve wordpress
[admin@web html]$ sudo chmod -R 755 /var/www/html/
[admin@web html]$ sudo chown -R apache:apache /var/www/html/
```
```bash=
#On ouvre le port 3306 sur le serveur WEB
[admin@web html]$ sudo firewall-cmd --add-port=3306/tcp --permanent
Success
[admin@web ~]$ sudo firewall-cmd --reload
[sudo] password for admin:
success
```
```bash=
# On modifie le fichier VIM qu’on a copié pour avoir wp-confif.php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'datacesi' );

/** MySQL database username */
define( 'DB_USER', 'admindata' );


/** MySQL database password */
define( 'DB_PASSWORD', 'admindata' );

/** MySQL hostname */
define( 'DB_HOST', 'db.tp2.cesi' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

```

On vérifie ensuite via le navigateur : http://10.99.99.11
La page WordPress s'affiche ! 

## III. Reverse Proxy

```bash=
#installer serveur NGINX
[admin@rp ~]$ sudo yum install epel-release -y
[sudo] password for admin:
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirroir.wptheme.fr
 * extras: mirrors.standaloneinstaller.com
[…] Complete ! 
[admin@rp ~]$ sudo yum install nginx -y
[…] Complete
```
On modifie le fichier /etc/nginx/nginx.conf
```bash=
#Configurer NGINX pour qu’il puisse renvoyer vers le serveur web : 
server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  web.cesi;
        root         /usr/share/nginx/html;
        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
        location / {
                proxy_pass      http://10.99.99.11:80/;
        }
        error_page 404 /404.html;
        location = /404.html {
        }
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```
```bash=
#On ouvre le port 80 sur le SRVRP
sudo firewall-cmd --add-port=80/tcp –permanent
sudo firewall-cmd –reload
```
On test ensuite sur le navigateur : http://10.99.99.13
La page WordPress s'affiche ! 
```bash=
#On modifie le fichier hosts dans l’etc 
# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
	10.99.99.13		web.cesi
```
On test sur le navigateur : http://web.cesi
La page WordPress s'affiche ! 

## IV. Un peu de sécu

#### Fail2Ban
On installe Fail2Ban
```bash=
 sudo yum install epel-release
[sudo] password for admin:
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: ftp.rezopole.net
 * epel: mirror.bytemark.co.uk [...]
 Complete
 
 sudo yum install epel-release
 [...]
 Dependency Installed:
  fail2ban-firewalld.noarch 0:0.11.1-10.el7     fail2ban-sendmail.noarch 0:0.11.1-10.el7     fail2ban-server.noarch 0:0.11.1-10.el7
  systemd-python.x86_64 0:219-78.el7_9.2

Complete!
```
```bash=
[admin@web ~]$ sudo systemctl start fail2ban
[admin@web ~]$ sudo systemctl enable fail2ban
Created symlink from /etc/systemd/system/multi-user.target.wants/fail2ban.service to /usr/lib/systemd/system/fail2ban.service.
[admin@web ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-01-08 11:41:33 CET; 10s ago
     Docs: man:fail2ban(1)
 Main PID: 1781 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─1781 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start

Jan 08 11:41:33 web.tp2.cesi systemd[1]: Starting Fail2Ban Service...
Jan 08 11:41:33 web.tp2.cesi systemd[1]: Started Fail2Ban Service.
Jan 08 11:41:33 web.tp2.cesi fail2ban-server[1781]: Server ready
[admin@web ~]$
```
On crée un fichier dans /etc/fail2ban pour mettre en place la config

```bash=
[admin@web fail2ban]$ sudo vim jail.local
### On admet ces paramètres dans le fichier de conf créé

# Ban hosts for 3 minutes 3 trys:
ignoreip = 127.0.0.1/8
bantime = 180
maxretry = 3

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
```
On restart le service
```bash=
[admin@web fail2ban]$ sudo systemctl restart fail2ban
```
J'essaie de me connecter en SSH
```bash=
[admin@web fail2ban]$ sudo tail -f /var/log/fail2ban.log
2021-01-08 11:46:52,508 fail2ban.jail           [1828]: INFO    Creating new jail 'sshd'
2021-01-08 11:46:52,528 fail2ban.jail           [1828]: INFO    Jail 'sshd' uses systemd {}
2021-01-08 11:46:52,528 fail2ban.jail           [1828]: INFO    Initiated 'systemd' backend
2021-01-08 11:46:52,530 fail2ban.filter         [1828]: INFO      maxLines: 1
2021-01-08 11:46:52,530 fail2ban.filtersystemd  [1828]: INFO    [sshd] Added journal match for: '_SYSTEMD_UNIT=sshd.service + _COMM=sshd'
2021-01-08 11:46:52,589 fail2ban.filter         [1828]: INFO      maxRetry: 3
2021-01-08 11:46:52,589 fail2ban.filter         [1828]: INFO      encoding: UTF-8
2021-01-08 11:46:52,589 fail2ban.filter         [1828]: INFO      findtime: 600
2021-01-08 11:46:52,589 fail2ban.actions        [1828]: INFO      banTime: 180
2021-01-08 11:46:52,591 fail2ban.jail           [1828]: INFO    Jail 'sshd' started
2021-01-08 11:48:20,198 fail2ban.filter         [1828]: INFO    [sshd] Found 10.99.99.1 - 2021-01-08 11:48:20
2021-01-08 11:48:20,199 fail2ban.filter         [1828]: INFO    [sshd] Found 10.99.99.1 - 2021-01-08 11:48:20
2021-01-08 11:48:26,934 fail2ban.filter         [1828]: INFO    [sshd] Found 10.99.99.1 - 2021-01-08 11:48:26
2021-01-08 11:48:26,955 fail2ban.actions        [1828]: NOTICE  [sshd] Ban 10.99.99.1
Connection reset by 10.99.99.11 port 22
```
J'ai été déconnecté, le fail2ban est fonctionnel et le fichier de log montre bien l'activité

Je peux utiliser ces commandes pour voir qui est ban et desban les ip
```bash=
sudo fail2ban-client set sshd unbanip 10.99.99.1
sudo fail2ban-client status sshd
```


#### Monitoring 
A) Installation de Netdata

```bash=
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
[...]
  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata                          .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed and running now!  -'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  enjoy real-time performance and health monitoring...

 OK
```
On ouvre ensuite le port 19999
```bash=
[admin@rp ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[admin@rp ~]$ sudo firewall-cmd --reload
success
[admin@rp ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 80/tcp 443/tcp 19999/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
On test sur le navigateur --> La page netdata s'affiche parfaitement

B) Altering

On récupère le Webhook URL donnée par discord sur notre serveur 
```bash=
https://discord.com/api/webhooks/797069306243579955/IbwShpMRfu4a7vUOazKTqViRQNwWLytTCDyKDI1Aw2o4ZQf8pqcPVyEZJhXUKLsfQw1Y
```
On configure ensuite le fichier pour activer discord

```bash=
sudo /etc/netdata/edit-config health_alarm_notify.conf 

#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/797069306243579955/IbwShpMRfu4a7vUOazKTqViRQNwWLytTCDyKDI1Aw2o4ZQf8pqcPVyEZJhXUKLs
fQw1Y"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"                                                                                                      
#------------------------------------------------------------------------------
```
On teste maintenant les altertes 
```bash=
#Je me connecte avec l'utilisateur netdata
[admin@rp health.d]$ sudo su -s /bin/bash netdata

bash-4.2$ /usr/libexec/netdata/plugins.d/alarm-notify.sh test 

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-01-08 16:03:04: alarm-notify.sh: INFO: sent discord notification for: rp.tp2.cesi test.chart.test_alarm is WARNING to 'alarms'
2021-01-08 16:03:04: alarm-notify.sh: INFO: sent email notification for: rp.tp2.cesi test.chart.test_alarm is WARNING to 'root'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-01-08 16:03:05: alarm-notify.sh: INFO: sent discord notification for: rp.tp2.cesi test.chart.test_alarm is CRITICAL to 'alarms'
2021-01-08 16:03:05: alarm-notify.sh: INFO: sent email notification for: rp.tp2.cesi test.chart.test_alarm is CRITICAL to 'root'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-01-08 16:03:05: alarm-notify.sh: INFO: sent discord notification for: rp.tp2.cesi test.chart.test_alarm is CLEAR to 'alarms'
2021-01-08 16:03:05: alarm-notify.sh: INFO: sent email notification for: rp.tp2.cesi test.chart.test_alarm is CLEAR to 'root'
# OK
# Je reçois bien les notifications sur discord dans le channel Général de mon serveur
```
On crée maintenant les alertes pour la RAM et les disques à 75% used
Pour ça il faut créer un nouveau fichier ici  /lib/netdata/conf.d/health.d

```bash=
#Pour la RAM
[admin@rp health.d]$ sudo vim ram75.conf 
#Dans le fichier : 
 alarm: ram_usage
 on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
 warn: $this > 75
 crit: $this > 90
 info: The percentage of RAM used by the system.
 #A 75% une alerte sera lancé, une vérification est faite toutes les minutes.
```
On met à jour les alertes 
```bash=
sudo netdatacli reload-health 
```
On va ensuite sur le navigateur netdata / alarms / all / system - ram
Notre alerte pour une utilisation à 75% est apparue

Pour le disque même manip dans un nouveau fichier
```bash=
[admin@rp health.d]$ cat disks75.conf
template: disk_full_percent
      on: disk.space
      os: linux freebsd
   hosts: *
families: !/dev !/dev/* !/run !/run/* *
    calc: $used * 100 / ($avail + $used)
   units: %
   every: 1m
    warn: $this > 75
    crit: $this > 95
   delay: up 1m down 15m multiplier 1.5 max 1h
    info: current disk space usage
      to: sysadmin
```
On met à jour les alertes
```bash=
sudo netdatacli reload-health 
```
On va ensuite vérifier nos alarmes sur le navigateur. La nouvelle alterte est bien présente.

#### Interface Web
On installe Cockpit 
```bash=
[admin@rp health.d]$ sudo yum install -y cockpit
[...]
  gnutls.x86_64 0:3.3.29-9.el7_6                                   gsettings-desktop-schemas.x86_64 0:3.28.0-3.el7
  json-glib.x86_64 0:1.4.2-2.el7                                   libmodman.x86_64 0:2.0.1-8.el7
  libproxy.x86_64 0:0.4.11-11.el7                                  nettle.x86_64 0:2.7.1-8.el7
  trousers.x86_64 0:0.3.14-2.el7

Complete!
```
On ouvre le port 9090/yvp
```bash=
[admin@rp health.d]$ sudo firewall-cmd --add-port=9090/tcp --permanent
[sudo] password for admin:
success
[admin@rp health.d]$ sudo firewall-cmd --reload
success
[admin@rp health.d]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 80/tcp 443/tcp 19999/tcp 9090/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
On démarre maintenant le service cockpit
```bash=
[admin@rp health.d]$ sudo systemctl start cockpit
[admin@rp health.d]$ sudo systemctl enable cockpit
[admin@rp health.d]$ sudo systemctl status cockpit
● cockpit.service - Cockpit Web Service
   Loaded: loaded (/usr/lib/systemd/system/cockpit.service; static; vendor preset: disabled)
   Active: active (running) since Fri 2021-01-08 16:16:20 CET; 17s ago
     Docs: man:cockpit-ws(8)
 Main PID: 4095 (cockpit-ws)
   CGroup: /system.slice/cockpit.service
           └─4095 /usr/libexec/cockpit-ws

Jan 08 16:16:20 rp.tp2.cesi systemd[1]: Starting Cockpit Web Service...
Jan 08 16:16:20 rp.tp2.cesi remotectl[4081]: Generating temporary certificate using: sscg --quiet --lifetime 3650 --key-strength 2048...
Jan 08 16:16:20 rp.tp2.cesi remotectl[4081]: Error generating temporary dummy cert using sscg, falling back to openssl
Jan 08 16:16:20 rp.tp2.cesi remotectl[4081]: Generating temporary certificate using: openssl req -x509 -days 36500 -newkey rsa:2048 -...
Jan 08 16:16:20 rp.tp2.cesi systemd[1]: Started Cockpit Web Service.
Jan 08 16:16:20 rp.tp2.cesi cockpit-ws[4095]: Using certificate: /etc/cockpit/ws-certs.d/0-self-signed.cert
Hint: Some lines were ellipsized, use -l to show in full.
```
On vérifie maintenant depuis le navigateur host 

On accède à la page web qui nous demande de rentrer nos credentials.
On rentre les informations et on accède ici : https://10.99.99.13:9090/system