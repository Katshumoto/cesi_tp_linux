# TP4: DNS, Backup et Docker

## I.DNS

#### Mise en place 

Dans un premier temps on duplique la VM Template qu'on va appeler ns1.tp2.cesi et lui adresser 10.99.99.14 

```bash=
#En attendant de mettre en place le DNS
sudo vim /etc/hosts
#On donne le nom de domaine ns1.tp2.cesi
sudo vim /etc/hostname
```
On installe le paquet epel-release puis les paquets bind et bind-utils

```bash=
[admin@ns1 ~]$ sudo yum install epel-release -y
[sudo] password for admin:
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirrors.standaloneinstaller.com
 * extras: centos.crazyfrogs.org
 * updates: mirroir.wptheme.fr
base                               [...]                                            1/1
  Verifying  : epel-release-7-11.noarch                                                                          1/1

Installed:
  epel-release.noarch 0:7-11

Complete!

[admin@ns1 ~]$ sudo yum install bind
[...]
Installed:
  bind.x86_64 32:9.11.4-26.P2.el7_9.3

Dependency Installed:
  audit-libs-python.x86_64 0:2.8.5-4.el7  checkpolicy.x86_64 0:2.5-8.el7              libcgroup.x86_64 0:0.41-21.el7
  libsemanage-python.x86_64 0:2.5-14.el7  policycoreutils-python.x86_64 0:2.5-34.el7  python-IPy.noarch 0:0.75-6.el7
  python-ply.noarch 0:3.4-11.el7          setools-libs.x86_64 0:3.3.8-4.el7

Complete!

[admin@ns1 ~]$ sudo yum install bind-utils
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: ftp.rezopole.net
 * epel: mirror.freethought-internet.co.uk
 * extras: centos.crazyfrogs.org
 * updates: mirroir.wptheme.fr
Package 32:bind-utils-9.11.4-26.P2.el7_9.3.x86_64 already installed and latest version
Nothing to do
```
On va maintenant configurer les ports 53/tpc et 53/udp

```bash=
[admin@ns1 ~]$ sudo firewall-cmd --add-port=53/tcp --permanent
success
[admin@ns1 ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[admin@ns1 ~]$ sudo firewall-cmd --reload
success
[admin@ns1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp 53/tcp 53/udp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
On configure le fichier /etc/named.conf

```bash=
[admin@ns1 ~]$ sudo vim /etc/named.conf
#Fichier de conf : 

//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
// See the BIND Administrator's Reference Manual (ARM) for details about the
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

options {
	listen-on port 53 { 127.0.0.1; 10.99.99.14; };
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	recursing-file  "/var/named/data/named.recursing";
	secroots-file   "/var/named/data/named.secroots";
	allow-query     { 10.99.99.0/24; };

	/* 
	 - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
	 - If you are building a RECURSIVE (caching) DNS server, you need to enable 
	   recursion. 
	 - If your recursive DNS server has a public IP address, you MUST enable access 
	   control to limit queries to your legitimate users. Failing to do so will
	   cause your server to become part of large scale DNS amplification 
	   attacks. Implementing BCP38 within your network would greatly
	   reduce such attack surface 
	*/
	recursion yes;

	dnssec-enable yes;
	dnssec-validation yes;

	/* Path to ISC DLV key */
	bindkeys-file "/etc/named.root.key";

	managed-keys-directory "/var/named/dynamic";

	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
	type hint;
	file "named.ca";
};

zone "tp2.cesi" IN {
         
         type master;
        
         file "/var/named/tp2.cesi.db";

         allow-update { none; };
};

zone "99.99.10.in-addr.arpa" IN {
          
          type master;
          
          file "/var/named/99.99.10.db";
         
          allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

```
On configure maintenant le fichier /var/named/tp2.cesi.db
```bash=
[admin@ns1 var]$ sudo vim /var/named/tp2.cesi.db

$TTL    604800
@   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      ns1.tp2.cesi.

ns1 IN  A       10.99.99.14
web     IN  A       10.99.99.11
db     IN  A       10.99.99.12
rp     IN  A       10.99.99.13
```
Enfin, on va configurer le fichier 99.99.10.db
```bash=
[admin@ns1 var]$ sudo vim /var/named/99.99.10.db

$TTL    604800
@   IN  SOA     ns1.tp2.cesi. root.tp2.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp2.cesi.

14.99.99.10 IN PTR ns1.tp2.cesi.

;PTR Record IP address to HostName
11      IN  PTR     web.tp2.cesi.
12      IN  PTR     db.tp2.cesi.
13      IN  PTR     rp.tp2.cesi.
```

On démarre ensuite le service 

```bash=
[admin@ns1 ~]$ sudo systemctl start named
[sudo] password for admin:
[admin@ns1 ~]$ sudo systemctl enable named
[admin@ns1 ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-01-09 11:44:10 CET; 57s ago
 Main PID: 1054 (named)
   CGroup: /system.slice/named.service
           └─1054 /usr/sbin/named -u named -c /etc/named.conf

Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './DNSKEY/IN': 2001:500:200::b#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './NS/IN': 2001:500:200::b#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './DNSKEY/IN': 2001:7fe::53#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './NS/IN': 2001:7fe::53#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './DNSKEY/IN': 2001:500:12::d0d#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './NS/IN': 2001:500:12::d0d#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './DNSKEY/IN': 2001:7fd::1#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: network unreachable resolving './NS/IN': 2001:7fd::1#53
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: managed-keys-zone: Key 20326 for zone . acceptance timer complete: ...usted
Jan 09 11:44:10 ns1.tp2.cesi named[1054]: resolver priming query complete
Hint: Some lines were ellipsized, use -l to show in full.
```
#### Test

Test pour la résolution DNS depuis web.tp2.cesi qui possède dig 

```bash=
[admin@web ~]$ dig web.tp2.cesi @10.99.99.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> web.tp2.cesi @10.99.99.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48672
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web.tp2.cesi.                  IN      A

;; ANSWER SECTION:
web.tp2.cesi.           604800  IN      A       10.99.99.11

;; AUTHORITY SECTION:
tp2.cesi.               604800  IN      NS      ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.           604800  IN      A       10.99.99.14

;; Query time: 0 msec
;; SERVER: 10.99.99.14#53(10.99.99.14)
;; WHEN: Sat Jan 09 11:48:09 CET 2021
;; MSG SIZE  rcvd: 91
```
Test en résolution inverse 
```bash=
[admin@web ~]$ dig -x 10.99.99.13 @10.99.99.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> -x 10.99.99.13 @10.99.99.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 26929
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;13.99.99.10.in-addr.arpa.      IN      PTR

;; ANSWER SECTION:
13.99.99.10.in-addr.arpa. 604800 IN     PTR     rp.tp2.cesi.

;; AUTHORITY SECTION:
99.99.10.in-addr.arpa.  604800  IN      NS      ns1.tp2.cesi.

;; ADDITIONAL SECTION:
ns1.tp2.cesi.           604800  IN      A       10.99.99.14

;; Query time: 0 msec
;; SERVER: 10.99.99.14#53(10.99.99.14)
;; WHEN: Sat Jan 09 11:48:56 CET 2021
;; MSG SIZE  rcvd: 112
```
#### Configuration du DNS de façon permanente

On configure le fichier /etc/resolv.conf
```bash=
# Generated by NetworkManager
search tp2.cesi
nameserver 10.99.99.14   
```
## II.Backup

## III.Docker
Installation de Docker 

Les yum-utils sont déjà installés sur la VM (sudo yum install -y yum-utils)

```bash=
[admin@web ~]$ sudo yum-config-manager \
> --add-repo \
> https://download.docker.com/linux/centos/docker-ce.repo
Loaded plugins: fastestmirror
adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
grabbing file https://download.docker.com/linux/centos/docker-ce.repo to /etc/yum.repos.d/docker-ce.repo
repo saved to /etc/yum.repos.d/docker-ce.repo
```
Installation de docker engine

```bash=
[admin@web ~]$ sudo yum install docker-ce docker-ce-cli containerd.io

[...]
Dependency Installed:
  audit-libs-python.x86_64 0:2.8.5-4.el7                     checkpolicy.x86_64 0:2.5-8.el7
  container-selinux.noarch 2:2.119.2-1.911c772.el7_8         docker-ce-rootless-extras.x86_64 0:20.10.2-3.el7
  fuse-overlayfs.x86_64 0:0.7.2-6.el7_8                      fuse3-libs.x86_64 0:3.6.1-4.el7
  libcgroup.x86_64 0:0.41-21.el7                             libseccomp.x86_64 0:2.3.1-4.el7
  libsemanage-python.x86_64 0:2.5-14.el7                     policycoreutils-python.x86_64 0:2.5-34.el7
  python-IPy.noarch 0:0.75-6.el7                             setools-libs.x86_64 0:3.3.8-4.el7
  slirp4netns.x86_64 0:0.4.3-4.el7_8

Complete!
```
On lance Docker
```bash=
[admin@web ~]$ sudo systemctl start docker
```
On vérifie que docker est installé correctement 
```bash=
[admin@web ~]$ sudo docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete                                                                                          Digest: sha256:1a523af650137b8accdaed439c17d684df61ee4d74feac151b5b337bd29e7eec
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```
On essaye de lancer un conteneur Wordpress : 
```bash=
[admin@web ~]$ sudo docker run --name cesi-wp -d   -e WORDPRESS_DB_HOST=10.99.99.12   -e WORDPRESS_DB_USER=admindata   -e WORDPRESS_DB_PASSWORD=admindata   wordpress

[...]
5fe27c860d66130504f05dc3b468f4045ab3c1a9bd630e6345b5f4030e646e6e
```