# TD3 VPN

## I. CA

### Setup
#### EasyRSA

```bash=
# Récupération de EasyRSA
[admin@vpn ~]$ curl -SLO https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   639  100   639    0     0   1414      0 --:--:-- --:--:-- --:--:--  1416
100 48907  100 48907    0     0  51858      0 --:--:-- --:--:-- --:--:--  130k
#création fichier mkdir
[admin@vpn ~]$ mkdir ca
#On extrait l’archive EasyRSA
[admin@vpn ~]$ tar xvzf EasyRSA-3.0.8.tgz
EasyRSA-3.0.8/
EasyRSA-3.0.8/easyrsa […]
On déplace les fichiers : 
[admin@vpn ~]$ mv EasyRSA-3.0.8/* ca/
```
#### Initialisation de la CA
```bash=
#Création du fichier vars
[admin@vpn ca]$ cp vars.example vars
#On modifie le fichier vars
set_var EASYRSA_REQ_COUNTRY     "FR"                                                                                
set_var EASYRSA_REQ_PROVINCE    "Gironde"                                                                            
set_var EASYRSA_REQ_CITY        "Bordeaux"                                                                           
set_var EASYRSA_REQ_ORG         "CESI"                                                                               
set_var EASYRSA_REQ_EMAIL       "cesi@cesi.com"                                                                     
set_var EASYRSA_REQ_OU          "Bap"
```
On initialise une nouvelle PKI 
```bash=
[admin@vpn ca]$ ./easyrsa init-pki
Note: using Easy-RSA configuration from: /home/admin/ca/vars
init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /home/admin/ca/pki
```
On génère la clé privée et le certificat de la CA

```bash=
[admin@vpn ca]$ ./easyrsa build-ca nopass
Note: using Easy-RSA configuration from: /home/admin/ca/vars
Using SSL: openssl OpenSSL 1.0.2k-fips  26 Jan 2017
Generating RSA private key, 2048 bit long modulus
......+++
....................................................+++
e is 65537 (0x10001)
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:vpn.cesi
CA creation complete and you may now import and sign cert requests.
Your new CA certificate file for publishing is at:
/home/admin/ca/pki/ca.crt
```
## II. OpenVPN Server
#### Installation d'OpenVPN

```bash=
[admin@vpn ca]$ sudo yum install -y epel-release
[sudo] password for admin:
Loaded plugins: fastestmirror […] Complete
[admin@vpn ca]$ sudo yum install -y openvpn
[…] 
Dependency Installed:
  pkcs11-helper.x86_64 0:1.11-3.el7
Complete!
```
#### EasyRSA
On récupère EasyRSA aussi pour le serveur
```bash=
#Récupération EasyRSA pour le serveur  même étapes que pour le CA
[admin@vpn ~]$ curl -SLO https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.8/EasyRSA-3.0.8.tgz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   639  100   639    0     0   2529      0 --:--:-- --:--:-- --:--:--  2535
100 48907  100 48907    0     0  66279      0 --:--:-- --:--:-- --:--:--  548k
[admin@vpn ~]$ mkdir ovpn
[admin@vpn ~]$ tar xvzf EasyRSA-3.0.8.tgz
EasyRSA-3.0.8/
EasyRSA-3.0.8/easyrsa
[…]
EasyRSA-3.0.8/x509-types/serverClient
[admin@vpn ~]$ mv EasyRSA-3.0.8/* ovpn/
#Initialisation de la pki
[admin@vpn ovpn]$ ./easyrsa init-pki

init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /home/admin/ovpn/pki
```
#### DH - Diffie-Hellman
```bash=
#Création de la clé
[admin@vpn ovpn]$ ./easyrsa gen-dh
[…] ......+..... […]
DH parameters of size 2048 created at /home/admin/ovpn/pki/dh.pem
#On copie la clé dans le dossier openvpn
[admin@vpn ovpn]$ sudo cp ./pki/dh.pem /etc/openvpn/
```
#### Clé et certificat du serveur
On génère une clé et une demande de signature de CSR
```bash=
[admin@vpn ovpn]$ ./easyrsa gen-req cesi_vpn nopass
Using SSL: openssl OpenSSL 1.0.2k-fips  26 Jan 2017
Generating a 2048 bit RSA private key
[…]
-----
Common Name (eg: your user, host, or server name) [cesi_vpn]:vpn.cesi
Keypair and certificate request completed. Your files are:
req: /home/admin/ovpn/pki/reqs/cesi_vpn.req
key: /home/admin/ovpn/pki/private/cesi_vpn.key
#On copie la clé dans le dossier openvpn
[admin@vpn ovpn]$ sudo cp ~/ovpn/pki/private/cesi_vpn.key /etc/openvpn/
```
On signe la demande de signature de certificat du serveur
```bash=
[admin@vpn ca]$ ./easyrsa import-req ~/ovpn/pki/reqs/cesi_vpn.req cesi_vpn
Note: using Easy-RSA configuration from: /home/admin/ca/vars
Using SSL: openssl OpenSSL 1.0.2k-fips  26 Jan 2017
The request has been successfully imported with a short name of: cesi_vpn
You may now use this name to perform signing operations on this request.
 
[admin@vpn ca]$ ./easyrsa sign-req server cesi_vpn
Note: using Easy-RSA configuration from: /home/admin/ca/vars
Using SSL: openssl OpenSSL 1.0.2k-fips  26 Jan 201
You are about to sign the following certificate.
Please check over the details shown below for accuracy. Note that this request
has not been cryptographically verified. Please be sure it came from a trusted
source or that you have verified the request checksum with the sender.
Request subject, to be signed as a server certificate for 825 days:
subject=
    commonName                = vpn.cesi
Type the word 'yes' to continue, or any other input to abort.
  Confirm request details: yes
Using configuration from /home/admin/ca/pki/easy-rsa-1959.2unJRB/tmp.qRqSUn
Check that the request matches the signature
Signature ok
The Subject's Distinguished Name is as follows
commonName            :ASN.1 12:'vpn.cesi'
Certificate is to be certified until Apr 12 12:47:26 2023 GMT (825 days)
Write out database with 1 new entries
Data Base Updated
Certificate created at: /home/admin/ca/pki/issued/cesi_vpn.crt
```
On récupère dans la configuration du serveur VPN du certificat signé, ainsi que le certificat de la CA
```bash=
[admin@vpn ca]$ sudo cp ~/ca/pki/issued/cesi_vpn.crt /etc/openvpn
[admin@vpn ca]$ sudo cp ~/ca/pki/ca.crt /etc/openvpn
[admin@vpn ca]$ openvpn --genkey --secret ta.key
[admin@vpn ca]$ sudo cp ta.key /etc/openvpn/
```
On configure maintenant le serveur OpenVPN, pour cela il faut récupérer le modèle fourni avec le paquet OpenVPN, l'ouvrir sur VIM et lui apporter les modifications nécessaires
```bash=
[admin@vpn ca]$ sudo cp /usr/share/doc/openvpn-2.4.10/sample/sample-config-files/server.conf /etc/openvpn/cesi_vpn.conf
#On Edite le fichier de conf OpenVPN
sudo vim /etc/openvpn/cesi_vpn.conf
port 1194
proto udp
dev tun
ca ca.crt
cert cesi_vpn.crt
key cesi_vpn.key  # This file should be kept secret
dh dh.pem
server 10.8.0.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 208.67.220.220"
keepalive 10 120
tls-auth ta.key 0 # This file is secret
cipher AES-256-CBC
user nobody
group nobody
persist-key
persist-tun
status openvpn-status.log
verb 3
explicit-exit-notify 1
auth SHA256
```
On démarre ensuite le service OpenVPN
```bash=
[admin@vpn openvpn]$ sudo systemctl status openvpn@cesi_vpn
[admin@vpn openvpn]$ sudo systemctl enable openvpn@cesi_vpn
[sudo] password for admin:
Created symlink from /etc/systemd/system/multi-user.target.wants/openvpn@cesi_vpn.service to 
/usr/lib/systemd/system/openvpn@.service.
```
On vérifie enfin l'opérabilité du serveur 
```bash=
[admin@vpn openvpn]$ ip a
4: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN group default qlen 100
    link/none
    inet 10.8.0.1 peer 10.8.0.2/32 scope global tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::453f:b5c:6314:3cf2/64 scope link flags 800
       valid_lft forever preferred_lft forever
[admin@vpn openvpn]$ sudo systemctl status openvpn@cesi_vpn
openvpn@cesi_vpn.service - OpenVPN Robust And Highly Flexible Tunneling Application On cesi_vpn
   Loaded: loaded (/usr/lib/systemd/system/openvpn@.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-01-07 14:55:48 CET; 8min ago
 Main PID: 2203 (openvpn)
[admin@vpn openvpn]$ sudo ss -alupn
State       Recv-Q Send-Q             Local Address:Port                            Peer Address:Port
UNCONN      0      0                              *:1194                                       *:*                   users:(("openvpn",pid=2203,fd=6))
UNCONN      0      0                              *:68                                         *:*                   users:(("dhclient",pid=804,fd=6))
```
#### Configuration réseau

On autorise le forwarding IPV4
```bash=
[admin@vpn openvpn]$ echo 'net.ipv4.ip_forward = 1' | sudo tee -a /etc/sysctl.conf
net.ipv4.ip_forward = 1
```
On met en place les règles de Firewalling différentes via la zone "trusted"
```bash=
[admin@vpn openvpn]$ sudo firewall-cmd --permanent --zone=trusted --add-interface=tun0
Success
[admin@vpn openvpn]$ sudo firewall-cmd --permanent --add-service openvpn
Success

#Autorisation du masquerading
[admin@vpn openvpn]$ sudo firewall-cmd --permanent --zone=trusted --add-masquerade
Success
#Repérer le nom de l’interface NAT qui permet de joindre l’exterieur
[admin@vpn openvpn]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.8.0.0/24 via 10.8.0.2 dev tun0
10.8.0.2 dev tun0 proto kernel scope link src 10.8.0.1
10.99.99.0/24 dev enp0s8 proto kernel scope link src 10.99.99.20 metric 101
#Règle avancée pour autoriser les clients connectés au VPN d’être routés vers internet 
[admin@vpn openvpn]$ sudo firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s  10.8.0.0/24 -o enp0s3 -j MASQUERADE
Success
#On relance le Firewall
[admin@vpn openvpn]$ sudo firewall-cmd --reload
Success
```
## III. Client 
On crée des dossiers dédiés aux fichiers des clients 

```bash=
[admin@vpn openvpn]$ mkdir -p ~/openvpn-clients/files
[admin@vpn openvpn]$ mkdir ~/openvpn-clients/configs
[admin@vpn openvpn]$ mkdir ~/openvpn-clients/base
```
On récupère les informations du serveur pour les clients 
```bash=
[admin@vpn openvpn]$ sudo cp ta.key ~/openvpn-clients/base/
[admin@vpn openvpn]$ sudo cp /etc/openvpn/ca.crt ~/openvpn-clients/base/
```
On récupère le fichier de configuration 
```bash=
[admin@vpn openvpn]$ sudo cp /usr/share/doc/openvpn-2.4.10/sample/sample-config-files/client.conf ~/openvpn-clients/base/
```
On modifie le fichier de configuration
```bash=
[admin@vpn base]$ sudo vim client.conf
client
dev tun
proto udp
remote <10.99.99.20> 1194
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
cipher AES-256-CBC
verb 3
auth SHA256
key-direction 1
```
On crée le script pour permettre de générer simplement une clé et un certificat pour les clients
```bash=
[admin@vpn openvpn-clients]$ sudo vim gen_config.sh

FILES_DIR=$HOME/openvpn-clients/files
BASE_DIR=$HOME/openvpn-clients/base
CONFIGS_DIR=$HOME/openvpn-clients/configs

BASE_CONF=${BASE_DIR}/client.conf
CA_FILE=${BASE_DIR}/ca.crt
TA_FILE=${BASE_DIR}/ta.key

CLIENT_CERT=${FILES_DIR}/${1}.crt
CLIENT_KEY=${FILES_DIR}/${1}.key

# Test for files
for i in "$BASE_CONF" "$CA_FILE" "$TA_FILE" "$CLIENT_CERT" "$CLIENT_KEY"; do
    if [[ ! -f $i ]]; then
        echo " The file $i does not exist"
        exit 1
    fi

    if [[ ! -r $i ]]; then
        echo " The file $i is not readable."
        exit 1
    fi
done

# Generate client config
cat > ${CONFIGS_DIR}/${1}.ovpn <<EOF
$(cat ${BASE_CONF})
<key>
$(cat ${CLIENT_KEY})
</key>
<cert>
$(cat ${CLIENT_CERT})
</cert>
<ca>
$(cat ${CA_FILE})
</ca>
<tls-auth>
$(cat ${TA_FILE})
</tls-auth>
EOF
```
On rend le fichier exécutable
```bash=
[admin@vpn openvpn-clients]$ sudo chmod u+x ~/openvpn-clients/gen_config.sh
```
On génère une clé et une demande de signature pour le certificat client 
```bash=
[admin@vpn ovpn]$ ./easyrsa gen-req cesi_client nopass
Using SSL: openssl OpenSSL 1.0.2k-fips  26 Jan 2017
[…]
Common Name (eg: your user, host, or server name) [cesi_client]:vpn.cesi
Keypair and certificate request completed. Your files are:
req: /home/admin/ovpn/pki/reqs/cesi_client.req
key: /home/admin/ovpn/pki/private/cesi_client.key
[admin@vpn ovpn]$ sudo cp /home/admin/ovpn/pki/private/cesi_client.key ~/openvpn-clients/files/
``` 
On signe la demande de signature de certificat avec notre CA
```bash=
[admin@vpn ca]$ ./easyrsa import-req /home/admin/ovpn/pki/reqs/cesi_client.req cesi_clien
Note: using Easy-RSA configuration from: /home/admin/ca/vars
Using SSL: openssl OpenSSL 1.0.2k-fips  26 Jan 2017
The request has been successfully imported with a short name of: cesi_client
You may now use this name to perform signing operations on this request.

[admin@vpn ca]$ ./easyrsa sign-req client cesi_client
[…]
Certificate created at: /home/admin/ca/pki/issued/cesi_client.crt
```
On récupère le certificat du client signé. Il faut récupérer le fichier cesi_client.ovpn sur le host - Installer en client VPN et utiliser ce fichier pour se connecter au serveur.

```bash=
[admin@vpn ca]$ sudo cp ~/ca/pki/issued/cesi_client.crt ~/openvpn-clients/files
#Problème de droit suite à une erreur de commande SUDO, pour y remédier 
[admin@vpn configs]$ sudo chown admin:admin . -R
[admin@vpn openvpn-clients]$ ./gen_config.sh cesi_client
[admin@vpn openvpn-clients]$ ls configs/
cesi_client.ovpn
```
Pour se connecter depuis un client Linux, on duplique et configure la VM template

```bash=
[admin@vpn openvpn-clients]$ sudo yum install -y openvpn
[...] Complete!
#On copie ensuite le fichier cesi_client.ovpn sur la machine client
[admin@vpn configs]$ ls
cesi_client.ovpn
[admin@vpn configs]$ scp cesi_client.ovpn admin@10.99.99.21:cesi_client.ovpn
The authenticity of host '10.99.99.21 (10.99.99.21)' can't be established.
ECDSA key fingerprint is SHA256:Eled7oadNSPu/nyVv7KulP1UEf/kyvZsFVSGONuzUQo.
ECDSA key fingerprint is MD5:33:bc:6a:db:b2:14:5c:c8:45:b5:4f:27:67:e3:62:6d.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.99.99.21' (ECDSA) to the list of known hosts.
admin@10.99.99.21's password:
cesi_client.ovpn
#Le fichier est bien sur la nouvelle machine 
[admin@vpnclient ~]$ ls
cesi_client.ovpn

[admin@vpnclient ~]$ sudo openvpn cesi_client.ovpn
Fri Jan  8 11:10:33 2021 OpenVPN 2.4.10 x86_64-redhat-linux-gnu [Fedora EPEL patched] [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Dec  9 2020 [...]
```
